<?php

declare(strict_types=1);

namespace App\Task2;

class Book
{
    protected string $title;
    protected int $price;
    protected int $pagesNumber;

    public function __construct($title, $price, $pagesNumber)
    {
        $this->title = $title;
        if ($price < 0) {
            throw new \Exception("Value must be more than zero or equal!");
        }
        $this->price = $price;
        if ($pagesNumber < 0) {
            throw new \Exception("Value must be more than zero or equal!");
        }
        $this->pagesNumber = $pagesNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}
